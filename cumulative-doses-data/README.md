
Data is refreshed once in a few days.

# Format

## Granular doses

doses-DATE.csv contains the following attributes:

- id
- doses
- state_id
- district_id
- male_doses
- female_doses
- today_male
- today_female
- today_doses
- today_others
- others_doses
- difference_today, from 18th June
- difference_total, from 18th June

## District-wise recent reports

state-districts-doses-DATE.csv contains the following attributes:

state-wise sheets. Each state sheet contains:

- state_id
- state_name
- district_id
- district_name
- difference_today
- difference_total

Aggregated state values will have "0" as `district_id` value and "-1" as `district_name` value.
