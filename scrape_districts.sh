#!/bin/bash

start=$(date -d "$1 + 1 day" +%F)
echo "\tdistrict $3"
curl https://api.cowin.gov.in/api/v1/reports/v2/getPublicReports\?state_id\=$2\&district_id=$3\&date\=$start > districts/$start-$2-district-$3.json
# sleep a second after each district
sleep 1
