#!/usr/bin/env python
# coding: utf-8

# Import dependencies
import numpy as np
import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sqlalchemy import create_engine
import openpyxl as pxl

engine = create_engine('sqlite:///vaccines.db', echo=False)


def extract_table_to_file(_date):
    """Save doses table to doses-DATE.csv

    Args:
        _date (string): Date of latest fetch
    """
    df = pd.read_sql_table('doses', con=engine)
    df.to_csv(f'cumulative-doses-data/doses-{_date}.csv', index=False)


def make_ordinal(n):
    '''
    Convert an integer into its ordinal representation::

        make_ordinal(0)   => '0th'
        make_ordinal(3)   => '3rd'
        make_ordinal(122) => '122nd'
        make_ordinal(213) => '213th'

    https://stackoverflow.com/a/50992575
    '''
    n = int(n)
    suffix = ['th', 'st', 'nd', 'rd', 'th'][min(n % 10, 4)]
    if 11 <= (n % 100) <= 13:
        suffix = 'th'
    return str(n) + suffix

# ## Set today date and read data
today = sys.argv[1]
_d = pd.to_datetime(today, format='%Y-%m-%d').strftime('%B')
today_str = f"{make_ordinal(today.split('-')[-1])} {_d}"

extract_table_to_file(today)

data = pd.read_csv(f'cumulative-doses-data/doses-{today}.csv')
print(data.shape)


def get_identifiers():
    """Get state and district identifiers from database.

    Returns:
        dataframe: state, district names and IDs
    """
    return pd.read_sql_table('identifiers', con=engine)


ids = get_identifiers()
states = ids[['state_id', 'state_name']].drop_duplicates()
districts = ids[['district_id', 'district_name']].drop_duplicates()

fillna_cols = ['today_male', 'today_female', 'today_doses', 'male_doses',
               'female_doses', 'others_doses', 'today_others']

for col in fillna_cols:
    data[col].fillna(0, inplace=True)

data['state_name'] = data['state_id'].apply(
    lambda x: states[states['state_id'] == x].state_name.values[0])


# ### Derive new columns
#
# - total doses: sum of doses given to male and female
# - programming friendly date column
# - daily ratio (male/female) of doses
# - daily and total difference in doses administered to female and male

data['total_doses'] = data['male_doses'] + data['female_doses']
data['_date'] = pd.to_datetime(data['date'], format='%Y-%m-%d')
data['daily_ratio'] = data['male_doses'] / data['female_doses']


def plot_total_difference():
    # State-wise small multiples of total doses difference between women and men
    g = sns.FacetGrid(
        data[(data['district_id'].isna())].sort_values(by='state_name'),
        col="state_name", sharey=False, col_wrap=4)

    g.map(sns.lineplot, "_date", "difference_total")
    g.set_xticklabels(rotation=90)

    for axes in g.axes:
        axes.axhline(0, ls='--', c='black')

    g.axes[0].text('2021-04-12', 50, 'more doses for women', color='gray')
    g.axes[0].text('2021-04-15', -1500, 'more doses for men', color='gray')

    g.fig.suptitle(f'Total difference b/w female, male doses - 8th March - {today_str} 2021')
    g.fig.subplots_adjust(top=.95, bottom=0.05)
    plt.savefig(f'plots/difference-total-{today}.pdf')
    plt.savefig(f'to_host/difference-total-latest.pdf')


def plot_daily_difference():
    # State-wise small multiples of daily doses difference between women and men
    g = sns.FacetGrid(
        data[(data['_date'] > '2021-04-26') & (data['district_id'].isna())].sort_values(by='state_name'),
        col="state_name", sharey=False, col_wrap=4)

    g.map(sns.lineplot, "_date", "difference_today")
    g.set_xticklabels(rotation=90)

    for axes in g.axes:
        axes.axhline(0, ls='--', c='black')

    g.axes[0].text('2021-05-12', 100, 'more doses for women', color='gray')
    g.axes[0].text('2021-05-15', -150, 'more doses for men', color='gray')

    g.fig.suptitle(f'Day-wise difference b/w female, male doses - 27th April to {today_str} 2021')
    g.fig.subplots_adjust(top=.95, bottom=0.05)
    plt.savefig(f'plots/difference-{today}.pdf')
    plt.savefig(f'to_host/difference-latest.pdf')


def prepare_today_data():
    data['district_id'].fillna(0, inplace=True)

    data['district_name'] = data['district_id'].apply(
        lambda x: districts[districts['district_id'] == x].district_name.values[0] if 
            districts[districts['district_id'] == x].district_name.size > 0 else '-1')


    today_data = data[data['date'] == today]
    today_data['district_name'] = today_data['district_id'].apply(
        lambda x: districts[districts['district_id'] == x].district_name.values[0] if 
            districts[districts['district_id'] == x].district_name.size > 0 else '-1')
    return today_data


def write_district_doses(today_data):
# Write to file
# Save district-wise dose differences to file
    f_out = f'cumulative-doses-data/state-districts-doses-{today}.xlsx'

    notes = {
        'column': ['state_id', 'state_name', 'district_id', 'district_name', 'difference_today', 'difference_total'],
        'description': [
            'State identifier',
            'State name',
            'District identifier',
            'District name',
            f'difference in (women-men) doses on {today_str}',
            f'difference in (women-men) doses from 8th March to {today_str}'
        ]
    }
    notes = pd.DataFrame(notes)
    notes.to_excel(f_out, 'notes', index=False)

    excel_book = pxl.load_workbook(f_out)

    state_cols = ['state_id', 'state_name', 'district_id', 'district_name', 'difference_today', 'difference_total']

    for state_id in range(1, 38):
        state_data = today_data[today_data['state_id'] == state_id]
        df_view = state_data[state_data['district_id'] != '-1'][state_cols].sort_values(by='difference_total')

        with pd.ExcelWriter(f_out, engine='openpyxl') as writer:
            writer.book = excel_book
            df_view.to_excel(writer, sheet_name=f'{df_view["state_name"].values[0]}', index=False)


if __name__ == '__main__':
    plot_total_difference()
    plot_daily_difference()
    today_data = prepare_today_data()
    write_district_doses(today_data)
