#!/bin/bash

# fetches state files and saves as JSON files to states/ directory
# Arguments: start and end date in that order
# `start date` value is exclusive, picks up from the next date.

bash date_range.sh 2021-08-09 2021-08-10

# extracts records from state JSON files
# fetches districts data from API
# extracts records from district JSON files
# dates as arguments
# ex: python pick_attributes.py 2021-06-17  ## executes for 17th June 2021
# ex: python pick_attributes.py 2021-06-17 2021-05-17 ## executes for 17th June 2021 and 17th May 2021
# python pick_attributes.py 2021-08-05
python pick_attributes.py 2021-08-10 2021-08-11

# uses updated database records created from above script
# in cumulative-doses-data/
  # creates doses-DATE.csv
  # creates states-districts-doses-DATE.xlsx
# in plots/
  # creates difference-DATE.pdf
  # creates difference-total-DATE.pdf
# in to_host/
  # creates difference-latest.csv
  # creates difference-total-latest.csv
python analysis.py 2021-08-11

# once complete... run surge in to_host/
cd to_host
convert -density 300 -quality 85 difference-latest.pdf difference-latest.png
convert -density 300 -quality 85 difference-total-latest.pdf difference-total-latest.png
rm difference-latest.pdf difference-total-latest.pdf
surge --domain doses-difference.surge.sh .
