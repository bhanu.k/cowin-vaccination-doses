# How to host

This directory is used to host statistics at https://doses-difference.surge.sh/

## Run surge

In terminal, run:

```bash
surge
```

