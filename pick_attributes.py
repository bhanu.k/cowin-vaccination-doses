"""Utilities to process CoWIN vaccination doses data."""

import sys
import json
import sqlite3
import glob
import subprocess
import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime, timedelta

engine = create_engine('sqlite:///vaccines.db', echo=False)


def extract_state_data(running_date):
    """Retrieve state records from JSON files."""
    for in_file in glob.glob(f"states/{running_date}-*.json"):
        print("running", in_file)
        data = json.load(open(in_file))
        # gets ['2021', '05', '08'] on split [:-1]
        # join the items to get the date
        _date = '-'.join(in_file.split('/')[1].split('-')[:-1])
        state_id = in_file.split('-')[-1].split('.')[0]

        vaccination = data['topBlock']['vaccination']
        male = vaccination['male']
        female = vaccination['female']
        today_male = vaccination['today_male'] if 'today_male' in vaccination else ''
        today_female = vaccination['today_female'] if 'today_female' in vaccination else ''
        today_doses = vaccination['today'] or ''

        others_doses = vaccination['others'] if 'others' in vaccination else ''
        today_others = vaccination['today_others'] if 'today_others' in vaccination else ''

        difference_today = today_female - today_male
        difference_total = female - male

        database = sqlite3.connect('vaccines.db')
        cursor = database.cursor()
        cursor.execute('''
            INSERT INTO doses(
                date,
                state_id,
                today_doses,
                male_doses,
                female_doses,
                others_doses,
                today_male,
                today_female,
                today_others,
                difference_today,
                difference_total
                )
                VALUES(?,?,?,?,?,?,?,?,?,?,?)
            ''', (_date, state_id, today_doses, male, female, others_doses, today_male,
                    today_female, today_others, difference_today, difference_total))
        database.commit()
        print("\tcompleted ", _date)


def get_district_data(running_date):
    """Fetch district data from API."""
    running_date = datetime.strptime(running_date, '%Y-%m-%d') - timedelta(days=1)
    running_date = running_date.strftime('%Y-%m-%d')
    for in_file in sorted(glob.glob(f"states/{running_date}-*.json")):
        print("running", in_file)
        _date = '-'.join(in_file.split('/')[1].split('-')[:-1])
        _state = in_file.split('-')[-1].split('.')[0]

        data = json.load(open(in_file))
        districts = data['getBeneficiariesGroupBy']
        district_ids = [item['id'] for item in districts]
        print(district_ids)
        for _id in district_ids:
            subprocess.run(["bash", "scrape_districts.sh", _date, _state, str(_id)])


def extract_district_data(running_date):
    """Retrieve district records from JSON files."""
    for in_file in sorted(glob.glob(f"districts/{running_date}-*.json")):
        print("running", in_file)
        data = json.load(open(in_file))
        # gets ['2021', '05', '08'] on split [:-1]
        # join the items to get the date
        state_id = in_file.split('-district-')[0][10:].split('-')[-1]
        _date = '-'.join(in_file.split('-district-')[0][10:].split('-')[:-1])
        district_id = in_file.split('-district-')[1].split('.')[0]

        vaccination = data['topBlock']['vaccination']
        male = vaccination['male']
        female = vaccination['female']
        today_male = vaccination['today_male'] if 'today_male' in vaccination else ''
        today_female = vaccination['today_female'] if 'today_female' in vaccination else ''
        today_doses = vaccination['today'] or ''

        others_doses = vaccination['others'] if 'others' in vaccination else ''
        today_others = vaccination['today_others'] if 'today_others' in vaccination else ''

        difference_today = today_female - today_male
        difference_total = female - male

        database = sqlite3.connect('vaccines.db')
        cursor = database.cursor()
        cursor.execute('''
            INSERT INTO doses(
                date,
                state_id,
                district_id,
                today_doses,
                male_doses,
                female_doses,
                others_doses,
                today_male,
                today_female,
                today_others,
                difference_today,
                difference_total
                )
                VALUES(?,?,?,?,?,?,?,?,?,?,?,?)
            ''', (_date, state_id, district_id, today_doses, male, female,
                    others_doses, today_male, today_female, today_others,
                    difference_today, difference_total))
        database.commit()
        print("\tcompleted ", _date, state_id, district_id)


def extract_state_district_ids():
    """Get state and district IDs.

    Writes to `identifiers` table in `vaccines.db` SQLite file.

    To identify the state and district IDs we can pick any specific recent date
    and extract the identifiers.

    This is a one-time utility. Doesn't have to be executed often as IDs don't change everyday.

    Args:
        None
    Returns
        None
    """
    cols = ['state_id', 'state_name', 'district_id', 'district_name']
    for in_file in glob.glob("states/2021-05-18-*.json"):
        print("running", in_file)
        _date = '-'.join(in_file.split('-')[:-1])
        _state = in_file.split('-')[-1].split('.')[0]

        data = json.load(open(in_file))
        districts = data['getBeneficiariesGroupBy']

        districts_out = pd.read_json(json.dumps(districts))
        districts_out[cols].to_sql('identifiers', con=engine, if_exists='append', index=False)


def get_doses():
    """Get doses data from database.

    Returns:
        dataframe: doses records
    """
    return pd.read_sql('select * from doses', con=engine)


def update_difference_columns():
    """Retrospectively update difference_today and difference_total columns.

    Uses doses table data and updates two columns in the same table.

    This is a one-time utility. Doesn't have to be executed often.
    """
    doses = get_doses()
    doses['today_male'] = doses['today_male'].replace('', 0)
    doses['today_female'] = doses['today_female'].replace('', 0)
    doses['difference_today'] = doses['today_female'].astype('int') - doses['today_male'].astype('int')
    doses['difference_total'] = doses['female_doses'].astype('int') - doses['male_doses'].astype('int')
    doses.to_sql('doses', engine, if_exists='replace', index=False)


if __name__ == '__main__':
    print("running main...")
    if len(sys.argv) < 2:
        print("Needs at least one date. Exiting...", len(sys.argv))
        sys.exit()
    for for_date in sys.argv[1:]:
        print("running for...", for_date)
        print("*" * 30)
        # extract state records from JSON files.
        extract_state_data(for_date)

        # Fetch districts data from API
        get_district_data(for_date)

        # # extract district records from JSON files.
        extract_district_data(for_date)
