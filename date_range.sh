#!/bin/bash

start=$1
end=$2

while ! [[ $start > $end ]]; do
    echo $start
    start=$(date -d "$start + 1 day" +%F)
    for (( i=1; i<=37; i++ )); do
      echo "\state $i"
      curl https://api.cowin.gov.in/api/v1/reports/v2/getPublicReports\?state_id\=$i\&date\=$start > states/$start-$i.json
      # sleep a second after each state
      sleep 1
    done
    # sleep two seconds after each day
    sleep 2
done
