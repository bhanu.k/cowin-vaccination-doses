# CoWIN vaccination doses

This application uses CoWIN API to fetch state and district vaccination records.

## Data

View [CoWIN vaccination doses spreadsheet](https://docs.google.com/spreadsheets/d/11R2o6Fknvcitbm-zw2SmI34UugsfQm9qJXZUTAKNiUo/edit#gid=1784120252).

Data is also available in Git LFS format: [states](states/), [districts](districts/), [cumulative doses](cumulative-doses-data/).

## Steps to get data

Review `trigger.sh` [file](trigger.sh).

Change dates as necessary. Then, run `trigger.sh` as follows:

```bash
bash trigger.sh
```

### Understand the script

#### Step 1 - Get state files

Change `start`, `end` date values. `start` value is exclusive, picks up from the next date.

```
bash date_range.sh start end
```

This creates day-wise state-wise JSON files. Ex: 2021-05-31-10.json for state with ID 10.

State IDs range between 1 and 37.

#### Step 2 - Process state records, fetch and process district records

Dates in `pick_attributes.py`'s `__main__` module ([code](https://gitlab.com/bhanu.k/cowin-vaccination-doses/-/blob/master/pick_attributes.py#L181)) are passed from `trigger.sh`. This will do the following:

1. extract state records from `*.json` files for the given date
	- This will update the `doses` table in `vaccines.db` file.
2. fetch data for all districts for a given date
	- This will trigger a bash script (`scrape_districts.sh`) and fetch data for all districts in a given state file (check `glob.glob()` call). Files are stored in `districts/` directory.
3. extract district records from `districts/*.json` files for the given date
	- This will update the `doses` table in `vaccines.db` file.

#### Overview of steps

1. Change dates in trigger.sh
2. Run `bash trigger.sh` from terminal
3. Export data from `vaccines.doses` table to `doses-DATE.csv`
4. Run `python analysis.py DATE` from terminal
5. Upload artifacts (`differences*-DATE.pdf`, `doses-DATE.csv`) to a new directory in https://drive.google.com/drive/u/0/folders/1XecIcXioeAUEokUg9sxzmcp8wMitIgRT
6. Run `surge` from terminal. For subdomain, use `doses-difference`
7. Confirm changes work in https://doses-difference.surge.sh

## Setup database

Details are saved in a `vaccines.db` SQLite file.

### doses table

`doses` table is used to save day-wise, state-wise, district-wise gender vaccination doses.

```sql
CREATE TABLE "doses" (
	"id"	BIGINT,
	"state_id"	BIGINT,
	"male_doses"	BIGINT,
	"female_doses"	BIGINT,
	"today_male"	BIGINT,
	"today_female"	BIGINT,
	"today_doses"	TEXT,
	"date"	TEXT,
	"district_id"	FLOAT,
	"others_doses"	BIGINT,
	"today_others"	TEXT,
	"difference_today"	BIGINT,
	"difference_total"	BIGINT
)
```

### identifiers table

`identifiers` table is used to save the identifiers for states, districts.

```sql
CREATE TABLE "identifiers" (
	"state_id"	INTEGER,
	"state_name"	TEXT,
	"district_id"	INTEGER,
	"district_name"	TEXT
)
```

## Hosting

Use `to_host` directory to host on Surge platform.
